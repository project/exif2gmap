The exif2gmap simply adds google map and marker of <a href=> linked 
jpeg whenever jpeg has geotag info in exif.

INSTALLATION INSTRUCTIONS

1. Extract this module to sites/[ all | {domain} ]/modules folder.
2. Login as the user who has administrator permissions (user/1).
3. Activate exif2gmap in the "Other" category,
   on the Administer >> Site building >> Modules page at admin/build/modules.

CONFIGURATION
1. First, you need to obtain Google Map API key from 
   http://code.google.com/apis/maps.

2. Set necessary information to exif2gmap setting page, which locates 
   on the admin/settings/exif2gmap.
	 The Googla Map API Key, size of map and enable flag are required.

3. Enable exif2gmap filter in a particular input format, go to
   Administer >> Site configuration >> Input formats
   at admin/settings/filters

4. Select configure for your choice of input format,
   and activate one of the exif2gmap filters.

KNOWN ISSUES
---
Please use the issue queue to report any issues you think need to be fixed.

COPYRIGHT AND ACKNOWLEDGEMENTS
---
The exif2gmap is copyright 2008 by Takashi Ikebe (iktaka@gmail.com). 
It is licensed under the GPL, a copy of which, is included.
